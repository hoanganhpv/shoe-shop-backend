<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\CartItemController;

Route::middleware('auth:sanctum')->group(function () {
    Route::get("/products", [ProductController::class, 'getAllProducts']);
    Route::post("/cart-items", [CartItemController::class, 'createNewCartItem']);
    Route::put("/cart-items/{cartItemId}", [CartItemController::class, 'updateCartItemQuantity']);
    Route::delete("/cart-items/{cartItemId}", [CartItemController::class, 'deleteCartItem']);
    Route::get("/cart-items", [CartItemController::class, 'getAllCartItems']);
});

Route::post('/register', [AuthController::class, 'register']);
Route::post('/login', [AuthController::class, 'login']);
