<?php

namespace App\Http\Controllers;
use Illuminate\support\Facades\Auth;
use Illuminate\support\Facades\Hash;
use Carbon\Carbon;
use App\Models\User;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(Request $request) {
        $request->validate([
            'name' => 'required|string',
            'email' => 'required|string|unique:users',
            'password' => 'required|string|min:6'
        ]);

        $user = new User([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password)
        ]);

        $user->save();
        return response()->json([
            'status' => 'OK',
            'code' => 200,
            'message' => 'User has been registed'], 200);
    }

    public function login(Request $request) {
        $request->validate([
            'email' => 'required',
            'password' => 'required|string'
        ]);

        $credentials = request(['email', 'password']);

        if(!Auth::attempt($credentials)) {
            return response()->json([
                'status' => 'UNAUTHORIZED',
                'code' => 401,
                'message' => 'Unauthorized'], 401);
        }

        $user = $request->user();
        $tokenResult = $user->createToken('Personal access token');
        $token = $tokenResult->plainTextToken;

        return response()->json([
            'status' => 'OK',
            'code' => 200,
            'message' => 'Login successfully!',
            'data' => [
            'user' => Auth::user(),
            'access_token' => $token,
            'token_type' => 'Bearer',
        ]]);
    }
}
