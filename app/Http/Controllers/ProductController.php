<?php

namespace App\Http\Controllers;
use App\Models\Product;

use Illuminate\Http\Request;

class ProductController extends Controller
{
    function getAllProducts() {
        return response()->json([
            'status' => 'OK',
            'code' => 200,
            'message' => 'Get all products successfully!',
            'data' => [
                'products' => Product::all()]
            ]
            , 200);
    }
}
