<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\CartItem;

class CartItemController extends Controller
{
    public function createNewCartItem(Request $request) {
        $product = Product::find($request->product_id);

        if (!$product) {
            return response()->json([
                'status' => 'NOT FOUND',
                'code' => 404,
                'message' => 'Product not found'], 404);
        }

        $request->validate([
            'product_id' => 'required|exists:products,id',
            'quantity' => 'required|integer|min:1'
        ]);

        $user = auth()->user();
        $product_id = $request->input('product_id');
        $quantity = $request->input('quantity');

        $existingCartItem = $user->cartItems()->where('product_id', $product_id)->first();

        if ($existingCartItem) {
            $existingCartItem->quantity += $quantity;
            $existingCartItem->save();
        } else {
            $newCartItem = new CartItem([
                'user_id' => $user->id,
                'product_id' => $product_id,
                'quantity' => $quantity,
            ]);
            $newCartItem->save();
        }

        return response()->json([
            'status' => 'OK',
            'code' => 201,
            'message' => 'Cart item created successfully'], 200);
    }

    public function updateCartItemQuantity(Request $request, $cartItemId)
    {
        $request->validate([
            'quantity' => 'required|integer|min:0',
        ]);

        $user = auth()->user();
        $quantity = $request->input('quantity');
        $cartItem = CartItem::where('user_id', $user->id)->find($cartItemId);

        if (!$cartItem) {
            return response()->json([
                'status' => 'NOT FOUND',
                'code' => 404,
                'message' => 'Cart item not found'], 404);
        }

        if ($quantity === 0) {
            $cartItem->delete();
        } else {
            $cartItem->quantity = $quantity;
            $cartItem->save();
        }

        return response()->json([
            'status' => 'Successfully!',
            'code' => 200,
            'message' => 'Cart item updated successfully'], 200);
    }

    public function deleteCartItem($cartItemId)
    {
        $user = auth()->user();
        
        $cartItem = CartItem::where('user_id', $user->id)->find($cartItemId);

        if (!$cartItem) {
            return response()->json([
                'status' => 'NOT FOUND',
                'code' => 404,
                'message' => 'Cart item not found'], 404);
        }

        $cartItem->delete();

        return response()->json([
            'status' => '200',
            'code' => 200,
            'message' => 'Cart item deleted successfully'], 200);
    }

    public function getAllCartItems()
    {
        $user = auth()->user();
        
        $cartItems = $user->cartItems()->with('product')->get();

        return response()->json([
            'status' => 'OK',
            'code' => 200,
            'message' => 'Get all cart item successfully!',
            'data' => [
                'cart_items' => $cartItems]
            ]
            , 200);
    }
}
