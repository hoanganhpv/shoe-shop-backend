# This is the entry skills testing project of GOLDEN OWL company.

- Executor: Phan Van Hoang Anh
- Project name: Shoe Shop
- Project description:
The project includes several features for displaying shoe products and adding them to the user's cart.
- Project status: Incomplete
- Progress:
 + Backend: 100%
 + Frontend: 50%

# Backend Technologies:

### Programming Language: PHP
### Framework: Laravel 10
### Architecture: Restful API
### Database: MySQL
### Features:
The project comprises APIs revolving around three main features:

+ Authentication and authorization
+ Product listing
+ Cart management

# APIs:
There are 7 APIs in total:

2 APIs that do not require authentication:
+ Register
+ Login

5 APIs that require Authentication:
+ Get all products
+ Add a new item to the cart
+ Update cart item quantity
+ Delete cart item
+ Get all cart items per user

# Installation and Running the Application:
To run the application, you need to have the following installed:

+ PHP 8.1 or Later
+ Composer
+ Laravel 10 application

    # Operational Steps:

+ Configure the database:
Install the MySQL database management system on the local system
Database connection information:
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3307
DB_DATABASE=shoe_shop
DB_USERNAME=root
DB_PASSWORD=

Once you have the project source, use the command line to navigate to the project folder and run the following commands sequentially:
+ composer update
+ php artisan migrate
+ php artisan db:seed --class=ProductTableSeeder
+ php artisan serve

If you have configured all the necessary systems, the application will run successfully.

# API details:

## Authentication/Authorizetion
+ register:
- URL: Host/api/register
- Request body: 
    {
        "email": ".......", -> String 
        "name": ".......", -> String
        "password": "......." -> String
    }

      Example: 
      {
        "email": "pvhoanganh240@gmail.com",
        "name": "Phan Văn Hoàng Anh",
        "password": "hoanganh"
      }
    
- Method: POST

+ Login
- URL: Host/api/login
- Request body: 
    {
        "email": ".......", -> String 
        "password": "......." -> String
    }

      Example: 
      {
        "email": "pvhoanganh240@gmail.com",
        "password": "hoanganh"
      }
- Method: POST
- Authorization: Bearer token (Require)

## Orther features

+ Get all products
- URL: Host/api/products
- Method: GET
- Authorization: Bearer token (Require)

+ Add new cart item
- URL: Host/api/cart-items
- Request body: 
    {
        "product_id": ..., -> Interger
        "quantity": ... -> Interger
    }

      Example: 
      {
        "product_id": 2,
        "quantity": 12
      }
- Method: POST
- Authorization: Bearer token (Require)

+ Update cart item quantity
- URL: Host/api/cart-items/{cartItemId}
- Request body: 
    {
        "quantity": ... -> Interger
    }

      Example: 
      {
        "quantity": 10
      }
- Method: PUT
- Authorization: Bearer token (Require)

+ Delete cart item
- URL: Host/api/cart-items/{cartItemId}
- Method: DELETE
- Authorization: Bearer token (Require)

+ Get all cart items of user
- URL: Host/api/cart-items
- Method: GET
- Authorization: Bearer token (Require)

    